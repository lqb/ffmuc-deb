FROM debian:buster
  
LABEL maintainer="lqb <lqb@gmx.de>"

RUN apt update \
 && apt install -y \
      bash \
      reprepro \
 && rm -rf \
      /var/lib/apt/lists

